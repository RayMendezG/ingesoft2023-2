from django.shortcuts import render, HttpResponse
from .models import *
from django.db.models import Count

# Create your views here.
def index(request):
    estudiantes_grupo1 = Estudiante.objects.filter(grupo = 1)
    estudiantes_grupo4 = Estudiante.objects.filter(grupo = 4)
    mismo_apellido = Estudiante.objects.filter(apellidos = "Mendoza")
    estudiantes_misma_edad = Estudiante.objects.filter(edad = 22)
    estudiantes_grupo3_misma_edad = Estudiante.objects.filter(grupo = 3, edad = 22)
    estudiantes = Estudiante.objects.all()

    return render(request, 'index.html', {'estudiantes_grupo1':estudiantes_grupo1, 
                                          'estudiantes_grupo4':estudiantes_grupo4,
                                          'mismo_apellido':mismo_apellido,
                                          'estudiantes_misma_edad':estudiantes_misma_edad, 
                                          'estudiantes':estudiantes,
                                          'estudiantes_grupo3_misma_edad':estudiantes_grupo3_misma_edad})